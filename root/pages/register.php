<!--

Assignment 2: Business Web Technology
Author: Stephen Mina
Student Number: 17072290

Purpose:
The purpose of this php file is to structure all the information we need the user to provide to register an account
Once they filled up the the form and clicked the submit button, the system will save the information into the database
and treat them as a registered use

-->

<?php
    require '../php/authentication.inc';

    // Check for any existing sessions
    session_start();

   // To inform users if they have registered successfully
   if (isset($_SESSION["register_message"]))
   {
        $register_message .= $_SESSION["register_message"];
        unset($_SESSION["register_message"]);
    }

    // FOR FUTURE PLAN, IMPLEMENT A MESSAGE to inform user that the profile picture they've provided has been uplaoded
    // if (isset($_SESSION["register_img_message"]))
    // {
    //      $user_message .= $_SESSION["register_image_message"];
    //      unset($_SESSION["register_image_message"]);
    //  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>
    Register
  </title>

  <link rel="stylesheet" type = "text/css" href="../css/main.css" >  <!-- Main style across the website -->
  <link rel="stylesheet" type = "text/css" href="../css/registerStyle.css" > <!-- Local Style for the register page -->

  <meta charset="UTF-8">
</head>

<body>


  <!-- Registration container -->
  <div class = "mainContainer">

    <!-- Menu Bar's container -->
    <div class = "menu-bar">

        <!-- Milestone  Reached logo -->
        <img src="../img/logo.png" alt="Milestone Reached logo " width = 49 height = 49>

        <!-- Navigation Bar -->
        <ul class = "menuList">
          <?php
            // If user is not logged in
            if (!(isset($_SESSION['user'])))
            {
           ?>
              <!-- Don't show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="register.php" > Register </a> </li>
              <li> <a href="login.php" > Login </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
          <?php
          }
          else // If user is logged in
          {
          ?>
              <!-- Show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="myProfile.php" > My Profile </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
              <li> <a href="logout.php" > Logout </a> </li>

          <?php
          }
          ?>
        </ul>
    </div>

    <!-- Welcome Texts -->
    <div class = "headers" >
      <h1>Register</h1>
    </div>
    <!-- End of Welcome Texts -->

    <div class = "registerForm" >
      <!-- Messages communicated via apache in $_SESSION[ ] to inform users that they have registered or not-->
      <?php echo "<p id='message'>" . $register_message . "</p>" ?>

      <!-- <form class="registerContent" method = "post" action="register.php"> -->
      <form id="usrform" action="../php/registerUser.php" method="post">

        <div class="reg-container">
          <!-- First Name -->
          <label for="firstName"><b>First Name:</b></label>
          <input type="text" placeholder="First Name" name="firstName" required>

          <!-- Last Name -->
          <label for="lastName"><b>Last Name:</b></label>
          <input type ="text" placeholder="Last Name" name="lastName" required>

          <!-- Email -->
          <label for="email"><b>Email:</b></label>
          <input type="text" placeholder="Enter Email" name="email" required>

          <!-- Password -->
          <label for="password"><b>Password:</b></label>
          <input type="password" placeholder="Password" name="password" required>

          <!-- Confirm Password -->
          <label for="confirmPsw"><b>Confirm Password:</b></label>
          <input type="password" placeholder="Confirm Password" name="confirmPsw" required>

          <!-- About -->
          <label for="about"><b> About Yourself:</b></label>
          <textarea id = "aboutText" rows="4" cols="50" name="about" form="usrform">Enter Something about yourself...</textarea>

          <!-- Contact -->
          <label for="contact"><b> Preferred Contacts/Social Media:</b></label>
          <textarea id = "contactText" rows="4" cols="50" name="contact" form="usrform">Enter Phone or Link to Social Media...</textarea>

          <!-- Photo -->
          <label for="profilePhoto"><b>Upload a Photo:</b></label>

          <input type="file" name="profilePhoto"  accept="image/*">


          <!-- <button type="submit" onclick="" >Register</button> -->
          <!-- When this button is clicked, it will call the action ../php/register.php -->
          <input type="submit" name = "reg-button" value="Register"/>
          <p>
            Already registered? <a href="login.php">Log in</a>
        </div>

    </form>
  </div>

</body>
</html>
