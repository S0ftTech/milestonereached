<!--

Assignment 2: Business Web Technology
Author: Stephen Mina
Student Number: 17072290

Purpose:
The purpose of this php file is to structure the logout page of milestonereached website after the user clicked the Logout button
within their profile
-->

<?php
  session_start();

  // Unset all of the session variables.
  $_SESSION = array();

  // If it's desired to kill the session, also delete the session cookie.
  // Note: This will destroy the session, and not just the session data!
  if (ini_get("session.use_cookies")) {
      $params = session_get_cookie_params();
      setcookie(session_name(), '', time() - 42000,
          $params["path"], $params["domain"],
          $params["secure"], $params["httponly"]
      );
  }

  // Finally, destroy the session.
  session_destroy();
 ?>


 ﻿<!DOCTYPE html>
 <html lang="en">
   <head>
       <meta charset="utf-8" />
       <!-- To show the users that they are in Logout page -->
       <title> Logged out</title>
       <link rel="stylesheet" type ="text/css" href ="../css/main.css"> <!-- Main style across the website -->
       <link rel="stylesheet" type="text/css" href="../css/logoutStyle.css"> <!-- Main style locally for logout page -->

   </head>

   <body>

     <div class = "mainContainer" >

       <!-- Menu Bar's container -->
       <div class = "menu-bar">

           <!-- Milestone  Reached logo -->
           <img src="../img/logo.png" alt="Milestone Reached logo " width = 49 height = 49>

           <!-- Navigation Bar -->
           <ul class = "menuList">
             <!-- Don't show logout and MyProfile page -->
             <li> <a href="../index.php" > Home </a> </li>
             <li> <a href="register.php" > Register </a> </li>
             <li> <a href="login.php" > Login </a> </li>
             <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
             <li> <a href="destinations.php" > Destinations </a> </li>
             <li> <a href="popularPost.php" > Popular posts </a> </li>
             <li> <a href="contactUs.php" > Contact Us </a> </li>
           </ul>
       </div>
       <!-- End of Menu Bar container -->

       <!-- To inform the user that they have logged out successfully -->
       <div class = "headers" >
         <h1>Milestone Reached</h1>
         <h2> Successfully logged out!! </h2>

         <!-- To help users to be directed to the hompage -->
         <h2> Click here to go back to Homepage: <a href = "../index.php"> Home </a></h2>
       </div>
     </div>

   </body>
 </html>
