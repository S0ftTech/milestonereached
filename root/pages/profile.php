<!--

Assignment 2: Business Web Technology
Author: Stephen Mina
Student Number: 17072290

Purpose:
The purpose of this php file is to structure the Popular post webpage of the milestonereached website
This will give the users or guests information about the the mostly viewed and liked blogs and within this blogs
there will be a link  that will direct them to the blog.php to show the associated information

Future Functionalities:
To have a working link to direct the users into the right blog page associated to the particular blog
-->

<?php

   // Create or find an existing session
    session_start();

    // To check if the session detects that there is user and show their information
    if (isset($_SESSION["user"]))
    {
        // echo  $_SESSION["email"];
        // echo  $_SESSION["firstname"];
        // echo $_SESSION["profilePhoto"];
        // echo $_SESSION["id"];
        // echo $_SESSION["rows"];
        // echo $_SESSION["title"];
        // echo $_SESSION["location"];
        // echo $_SESSION["summary"];
        // echo  $_SESSION["blogPhoto"];

     }

   // TO show any error messages within the web page if there are any
   if (isset($_SESSION["message"]))
   {
        $message .= $_SESSION["message"];
        unset($_SESSION["message"]);
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title> Profile</title>

    <link rel="stylesheet" type="text/css" href="../css/main.css"> <!-- Main style across the website -->
    <link rel="stylesheet" type="text/css" href="../css/profileStyle.css"> <!-- Main style locally for profile page -->

</head>

<body>
  <div class = "mainContainer">

    <!-- Menu Bar's container -->
    <div class = "menu-bar">

        <!-- Milestone  Reached logo -->
        <img src="../img/logo.png" alt="Milestone Reached logo " width = 49 height = 49>

        <!-- Navigation Bar -->
        <ul class = "menuList">
          <?php
            // If user is not logged in
            if (!(isset($_SESSION['user'])))
            {
           ?>
              <!-- Don't show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="register.php" > Register </a> </li>
              <li> <a href="login.php" > Login </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
          <?php
          }
          else // If user is logged in
          {
          ?>
              <!-- Show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="myProfile.php" > My Profile </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
              <li> <a href="logout.php" > Logout </a> </li>

          <?php
          }
          ?>
        </ul>
    </div>
    <!-- End of Menu Bar container -->

    <!-- To show the Profile Text within the page -->
    <h1> Profile </h1>

    <div class = "profileContainer">
      <!-- If the user does not have a profile photo provided -->
      <?php
        if ($_SESSION['profilePhoto'] == NULL)
        {
      ?>
      <!-- Set the default photo as their profile pic -->
      <img src = "../img/profile_imgs/defaultPhoto.jpg" alt="defaultPic" height="150" width="150" >
      <?php
        }
        // Else use their provided photo
        else
        {
      ?>
        <img src = "../img/profile_imgs/<?= $_SESSION['profilePhoto'] ?>" alt = "pic" height = "150" width = "150">
      <?php
        }
       ?>

       <!-- The system presents the user information who just logged in -->
      <div class = "userDetails">
        <h2> <?= $_SESSION['firstname'] ?> <?= $_SESSION['lastname'] ?> </h2>
        <h4> About myself: </h4>
          <p>
            <?= $_SESSION['about'] ?>
          </p>

        <div class = "contacts">
          <h4> Contacts: </h4>
          <p>
            <?= $_SESSION['contacts'] ?>
          </p>
        </div>

      </div>


      <!-- To display all the blogs that this user owns -->
      <div class = "blogCollection">
        <div class = "header">
          <h2 class = "header2" > Blogs: </h2>
        </div>

        <ul>
          <!-- FUTURE Functionality: the system will present the blogs created by this user-->
          <li>
              <img src="../img/blog_imgs/<?=$_SESSION["blogPhoto"]?>" alt = "foodAdv1" height="150" width="150">
              <h2> <?=$_SESSION["title"]?> </h2>
              <h3> <?=$_SESSION["location"]?> </h3>
              <p>
                <?=$_SESSION["summary"]?>
              </p>

              <a href = "blog.php"> Read More </a> <!-- Link of the blog -->
          </li>

          <li>
              <img src="../img/blog_imgs/disneyland.jpg" alt="disneyLand" height="150" width="150">
              <h2> Disney Land </h2>
              <h3> Shanghai, China </h3>
              <p>
                The First Disneyland in China. Where it is a land of fantasy
              </p>

              <a href = "blog2.php"> Read More </a> <!-- Link of the blog -->
          </li>

          <li>
              <img src="../img/blog_imgs/wkendGetaway.jpg" alt="melbAust" height="150" width="150">
              <h2> Weekend Getaway </h2>
              <h3> Melbourne, Australia </h3>
              <p>
                Walking by the streets
              </p>

              <a href = "blog3.php"> Read More </a> <!-- Link of the blog -->
          </li>


        </ul>
      </div>


    </div>
    <!-- end of profileContainer -->
  </div>
  <!-- end of mainContainer -->
</body>
</html>
