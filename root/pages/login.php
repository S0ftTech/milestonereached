<!--

Assignment 2: Business Web Technology
Author: Stephen Mina
Student Number: 17072290

Purpose:
The purpose of this php file is to structure the login page of milestonereached website
The login form will be presented to the users or guest by having an email and password fields
and a Login button
-->

<?php

   // Create or find an existing session
    session_start();

   // To inform the user whether he/she has logged in successfully or not
   if (isset($_SESSION["message"]))
   {
        $message .= $_SESSION["message"];
        unset($_SESSION["message"]);
    }

  // Destroy the message
  session_destroy();

?>

<!DOCTYPE html>
<html lang="en">
<head>

  <!-- To show the users that they are in Login page -->
  <title>
    Login
  </title>

  <link rel="stylesheet" type = "text/css" href="../css/main.css" >  <!-- Main style across the website -->
  <link rel="stylesheet" type = "text/css" href="../css/loginStyle.css" > <!-- Main style locally for login page -->

</head>

<body>
    <!-- Menu Bar's container -->
    <div class = "menu-bar">

      <!-- Milestone  Reached logo -->
      <img src="../img/logo.png" alt="Milestone Reached logo " width = 49 height = 49>
      <!-- Navigation Bar -->
        <ul class = "menuList">
          <?php
            // If user is not logged in
            if (!(isset($_SESSION['user'])))
            {
           ?>
              <!-- Don't show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="register.php" > Register </a> </li>
              <li> <a href="login.php" > Login </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
          <?php
          }
          else // If user is logged in
          {
          ?>
              <!-- Show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="myProfile.php" > My Profile </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
              <li> <a href="logout.php" > Logout </a> </li>

          <?php
          }
          ?>
        </ul>
    </div>
    <!-- End of Menu Bar container -->


    <!-- Login Welcome Texts -->
    <div class = "headers" >
      <h1>Login</h1>
    </div>
    <!-- End of  Login Welcome Texts -->


    <!-- Login Form -->
    <div class = "loginForm" >

      <!-- Register form that will submit information provided by the users to the database -->
      <form class="loginContent" action="../php/logincheck.php" method="post">

        <!-- Messages communicated via apache in $_SESSION[ ] if the user has successfully logged in or not-->
        <?php echo "<p id='message'>" . $message . "</p>" ?>

        <!-- Login structure with email and password-->
        <div class="container">
          <label for="email"><b>Email</b></label>
          <input type="text" placeholder="Email" name="email" required>

          <label for="password"><b>Password</b></label>
          <input type="password" placeholder="Password" name="password" required>

          <!-- FUTURE feature to remember the password of the users -->
          <!-- <label>
            <input type="checkbox" checked="checked" name="remember"> Remember me
          </label> -->
          <input type="submit" name = "login-button" value="Login"/>

          <!-- TO direct users to the registration page if they have not registered yet -->
          <div id = "help">
            <div class="createAccnt">
              <p> Don't have an account? <a href="register.php">Create Account</a> </p>
            </div>

            <!-- FUTURE feature: to create a forgot password for users having trouble in loggin in their account  -->
            <!-- <div class = "forgotPass">
              <p> Can't Remember password? <a href="#">Forgot Password</a> </p> -->

          </div>
        </div>


          <!-- <div id="help">
            <div class="createAccnt"> <p> Don't have an account? <a href="register.php">Create Account</a> </p></div>
            <div class="forgotPass"> <p> Can't Remember password? <a href="#">Forgot Password</a> </p></div>
          </div> -->

      </form>
    </div>
</body>
</html>
