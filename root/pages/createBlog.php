<!--

Assignment 2: Business Web Technology
Author: Stephen Mina
Student Number: 17072290

Purpose:
The purpose of this php file is to structure all the information that must be presented to a user
when they want to create a user into the webpage
-->


<?php

   // Create or find an existing session
    session_start();

   // To inform users if they have created a blog successfully
   if (isset($_SESSION["create_message"]))
   {
        $create_message .= $_SESSION["create_message"];
        unset($_SESSION["create_message"]);
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <!-- Name of the Webpage -->
    <title> Blog Creator</title>

    <link rel="stylesheet" type="text/css" href="../css/main.css"> <!-- Main style across the website -->
    <link rel ="stylesheet" type ="text/css" href="../css/createBlogStyle.css"> <!-- Local Style for the Create blog page -->

</head>

<body>

  <!-- Create Blog page container -->
  <div class = "mainContainer">

    <!-- Menu Bar's container -->
    <div class = "menu-bar">

        <!-- Milestone  Reached logo -->
        <img src="../img/logo.png" alt="Milestone Reached logo " width = 49 height = 49>

        <!-- Navigation Bar -->
        <ul class = "menuList">
          <?php
            // If user is not logged in
            if (!(isset($_SESSION['user'])))
            {
           ?>
              <!-- Don't show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="register.php" > Register </a> </li>
              <li> <a href="login.php" > Login </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
          <?php
          }
          else // If user is logged in
          {
          ?>
              <!-- Show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="myProfile.php" > My Profile </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
              <li> <a href="logout.php" > Logout </a> </li>

          <?php
          }
          ?>
        </ul>
    </div>
    <!-- End of Menu Bar container -->

    <!-- To show the users that they are on a page that they can create blogs-->
    <h1> Create Blog </h1>

    <!-- Create Blog form that will call creatBlog.php when create button is clicked-->
    <form id="blogform" class="createBlogForm" action="../php/createBlog.php" method="post">

      <div class = "create-container">
        <!-- Messages communicated via apache in $_SESSION[ ] to inform user if the blog has been created and stored in the database-->
        <?php echo "<p id='message'>" . $create_message . "</p>" ?>
        <!-- Title -->
        <label for="title"> <b> Title:</b> </label>
        <input type = "text" name = "title" required>

        <!-- Locations -->
        <label for="location"> <b> Location:</b> </label>
        <input type = "text" name = "location" required>

        <!-- Summary -->
        <label for="summary"> <b> Summary:</b> </label>
        <textarea rows="1" cols="50" name="summary" form="blogform" maxlength="58"></textarea>

        <!-- To share their story and experience of their travel -->
        <label for="story"> <b> Experience / Story:</b> </label>
        <textarea rows="4" cols="100" name="story" form="blogform"></textarea>

        <!-- To upload multiple pictures -->
        <label for="photoCollection"><b>Upload Photo/s:</b></label>
        <input type="file" name="photoCollection"  accept="image/*" multiple="multiple">


        <!-- To upload multiple videos -->
        <label for="videoCollection"><b>Upload Video/s:</b></label>
        <input type="file" name="videoCollection"  accept="video/*" multiple="multiple">



        <!-- To submit the form into the database -->
        <!-- <button type="create" class="createBtn">Create</button> -->
        <input type="submit" name = "create-button" value="Create"/>
      </div>

    </form>

  </div>

</body>
</html>
