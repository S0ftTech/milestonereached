<!--

Assignment 2: Business Web Technology
Author: Stephen Mina
Student Number: 17072290

Purpose:
The purpose of this php file is to structure all the information we need the user to update his/her information
The form that will be presented here will allow users to update their user information
-->

<?php
    require '../php/authentication.inc';

    // Check for any existing sessions
    session_start();

   // To inform users if they have updated their profile successfully
   if (isset($_SESSION["update_message"]))
   {
        $update_message .= $_SESSION["update_message"];
        unset($_SESSION["update_message"]);
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>
    Update profile
  </title>

  <link rel="stylesheet" type = "text/css" href="../css/main.css" >  <!-- Main style across the website -->
  <link rel="stylesheet" type = "text/css" href="../css/updateStyle.css" > <!-- Local Style for the Update Profile page -->

  <meta charset="UTF-8">
</head>

<body>


  <!-- Update profile container -->
  <div class = "mainContainer">

    <!-- Menu Bar's container -->
    <div class = "menu-bar">

        <!-- Milestone  Reached logo -->
        <img src="../img/logo.png" alt="Milestone Reached logo " width = 49 height = 49>

        <!-- Navigation Bar -->
        <ul class = "menuList">
          <?php
            // If user is not logged in
            if (!(isset($_SESSION['user'])))
            {
           ?>
              <!-- Don't show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="register.php" > Register </a> </li>
              <li> <a href="login.php" > Login </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
          <?php
          }
          else // If user is logged in
          {
          ?>
              <!-- Show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="myProfile.php" > My Profile </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
              <li> <a href="logout.php" > Logout </a> </li>

          <?php
          }
          ?>
        </ul>
    </div>

    <!-- Welcome Texts -->
    <div class = "headers" >
      <h1>Update Profile</h1>
    </div>
    <!-- End of Welcome Texts -->

    <div class = "updateForm" >
      <!-- Messages communicated via apache in $_SESSION[ ] -->
      <?php echo "<p id='message'>" . $update_message . "</p>" ?>

      <!-- <form class="updateContent" method = "post" action="updateProfile.php"> -->
      <form id=updateInfo class="updateContent" action="../php/updateUserProfile.php" method="post">

        <div class="reg-container">
          <!-- First Name -->
          <label for="firstName"><b>First Name:</b></label>
          <input type="text"  name="updateFirstname" value="<?=$_SESSION['firstname']?>">

          <!-- Last Name -->
          <label for="lastName"><b>Last Name:</b></label>
          <input type ="text" name="updateLastname" value="<?=$_SESSION['lastname']?>">

          <!-- Email -->
          <label for="email"><b>Email:</b></label>
          <input type="text" name="updateEmail" value="<?=$_SESSION['user']?>">

          <!-- Password -->
          <label for="password"><b>Password:</b></label>
          <input type="password" name="updatePassword" value="<?=$_SESSION["password"]?>">

          <!-- About -->
          <label for="about"><b> About Yourself:</b></label>
          <textarea id = "aboutText" rows="4" cols="50" name="updateAbout" form="updateInfo"><?=$_SESSION["about"]?></textarea>

          <!-- Contact -->
          <label for="contact"><b> Preferred Contacts/Social Media:</b></label>
          <textarea id = "contactText" rows="4" cols="50" name="updateContact" form="updateInfo" ><?=$_SESSION["contacts"]?></textarea>

          <!-- Photo -->
          <label for="profilePhoto"><b>Upload a Photo:</b></label>

          <input type="file" name="updatePhoto"  accept="image/*">


          <!-- <button type="submit" onclick="" >Update</button> -->
          <!-- When this button is clicked, it will call the action ../php/updateProfile.php -->
          <input type="submit" name = "update-button" value="Update"/>

        </div>

    </form>
  </div>

</body>
</html>
