<!--

Assignment 2: Business Web Technology
Author: Stephen Mina
Student Number: 17072290

Purpose:
The purpose of this php file is to structure the Geo chart to show the guests and users a chart. This chart
will provide the number of blogs that exist within a continent
-->

<?php
   // Create or find an existing session
    session_start();

   // Possible logout/can't authenticate message
   if (isset($_SESSION["message"]))
   {
        $message .= $_SESSION["message"];
        unset($_SESSION["message"]);
    }
?>

<!DOCTYPE html>

<html lang="en">
<head>
  <!-- To show the users that they are in Destinations page -->
  <title>
    Destinations
  </title>

  <link rel="stylesheet" type = "text/css" href="../css/main.css" > <!-- Main style across the website -->
  <link rel="stylesheet" type = "text/css" href="../css/destinationStyle.css">  <!-- Main style locally within this Destination webpage-->
  <!-- 2 External links to use google charts -->
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js?sensor=false&callback=initialize"></script>
  <script type="text/javascript" src="https://www.google.com/jsapi?sensor=false"></script>



</head>

<body>

    <!-- Menu Bar's container -->
    <div class = "menu-bar">

      <!-- Milestone  Reached logo -->
        <img src="../img/logo.png" alt="Milestone Reached logo " width = 49 height = 49>
        <!-- Navigation Bar -->
        <ul class = "menuList">
          <?php
            // If user is not logged in
            if (!(isset($_SESSION['user'])))
            {
           ?>
              <!-- Don't show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="register.php" > Register </a> </li>
              <li> <a href="login.php" > Login </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
          <?php
          }
          else // If user is logged in
          {
          ?>
              <!-- Show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="myProfile.php" > My Profile </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
              <li> <a href="logout.php" > Logout </a> </li>

          <?php
          }
          ?>
        </ul>
    </div>

        <!-- To show the Destination text: -->
        <div class = "header">
          <h1>Destinations:</h1>
        </div>


        <!-- Destination container -->
        <div class = "destination" >
          <h2> The countries that bloggers have visited so far </h2>
          <!-- This is to show the geo chart within the Destinations page -->
          <div id="continents_div" >

          </div>



        </div>

</body>

<!-- Accessing the javascript used to load the Google chart within this page -->
<script type="text/javascript" >
google.charts.load('current', {'packages':['geochart']});
// Note: you will need to get a mapsApiKey.
        // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
google.charts.setOnLoadCallback(drawRegionsMap);

// a Javascript function to display the Geo chart into the destionations page
function drawRegionsMap() {

// Set the world map to display these specific continents
var data = google.visualization.arrayToDataTable([
          ['Country', 'Blog Numbers'],
          ['Taiwan', 1],
          ['China', 1],
          ['Australia', 1],
          // ['Region Code', 'Continent', 'Blog Numbers'],
          // ['142', 'Asia', 2],
          // ['150', 'Europe', 0],
          // ['019', 'Americas', 0],
          // ['009', 'Oceania', 1],
          // ['002', 'Africa', 0]

          // FUTURE FUNCTIONALITY: A query to acquire the total number of blogs for each continent and place them into the Blog number row of the geo chart array
          <?php
          // $connection = get_mysql_connection();

          $query = "SELECT count(location) AS count, location FROM `userblogs` GROUP BY location";

            $result = mysql_query($query,$connection);

            while($row = mysqli_fetch_array($result))
            {
              // echo " ['$row['location'], $row['count']]  ";
              // This should return a 'Taiwan' and 1;
              echo "['".$row['location']."',".$row['count']."],";
              // If continent is Asia do $this
              // if($row['continent'] == 'Asia')
              // {
              //     ['142', 'Asia', $row['count']],
              // }
              // // If continent is Europe
              // else if($row['continent'] == 'Europe')
              // {
              //     ['150', 'Europe', $row['count']],
              // }
              // // If continent is Americas
              // else if($row['continent'] == 'Americas')
              // {
              //     ['019', 'Americas', $row['count']],
              // }
              // // If continent is Oceania
              // else if($row['continent'] == 'Oceania')
              // {
              //     ['009', 'Oceania', $row['count']],
              // }
              // // If continent is Africa
              // else
              // {
              //     ['002', 'Africa', $row['count']],
              // }
            }
          ?>

]);

// Set the world map to display the continents
var options = {};

// We will then load this chart to our webpage within continents_div
var chart = new google.visualization.GeoChart(document.getElementById('continents_div'));

chart.draw(data, options);
}
</script>
<!--
Reference: Google. "Visualization: GeoChart". Google Charts. https://developers.google.com/chart/interactive/docs/gallery/geochart

-->




</html>
