<!--

Assignment 2: Business Web Technology
Author: Stephen Mina
Student Number: 17072290

Purpose:
The purpose of this php file is to structure the options or settings that users will be able to do within their profile:
This includes:

Create Blog - to create a blog
Profile overview - to direct them to their profile page

FUTURE functionalities and features that could have implemented:
Updating their profile button - to direct them to the page where they can update their file
Followers - to list all their followers
Following - to list all of the users they are following
My blog - To list all the blogs that the users have created

-->
<?php

   // Create or find an existing session
    session_start();

   // To show any error messages within this page if there are any
   // if (isset($_SESSION["message"]))
   // {
   //      $message .= $_SESSION["message"];
   //      unset($_SESSION["message"]);
   //  }
?>

﻿<!DOCTYPE html>
<html lang="en">
  <head>
      <meta charset="utf-8" />
      <!-- To show the users that they are in My Profile page -->
      <title> My Profile</title>
      <link rel="stylesheet" type ="text/css" href ="../css/main.css"> <!-- Main style across the website -->
      <link rel="stylesheet" type="text/css" href="../css/myProfileStyle.css"> <!-- Main style locally for myProfile page -->
  </head>

  <body>
    <div class = "mainContainer" >
      <!-- Menu Bar's container -->
      <div class = "menu-bar">

          <!-- Milestone  Reached logo -->
          <img src="../img/logo.png" alt="Milestone Reached logo " width = 49 height = 49>

          <!-- Navigation Bar -->
          <ul class = "menuList">
            <?php
              // If user is not logged in
              if (!(isset($_SESSION['user'])))
              {
             ?>
                <!-- Don't show logout and MyProfile page -->
                <li> <a href="../index.php" > Home </a> </li>
                <li> <a href="register.php" > Register </a> </li>
                <li> <a href="login.php" > Login </a> </li>
                <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
                <li> <a href="destinations.php" > Destinations </a> </li>
                <li> <a href="popularPost.php" > Popular posts </a> </li>
                <li> <a href="contactUs.php" > Contact Us </a> </li>
            <?php
            }
            else // If user is logged in
            {
            ?>
                <!-- Show logout and MyProfile page -->
                <li> <a href="../index.php" > Home </a> </li>
                <li> <a href="myProfile.php" > My Profile </a> </li>
                <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
                <li> <a href="destinations.php" > Destinations </a> </li>
                <li> <a href="popularPost.php" > Popular posts </a> </li>
                <li> <a href="contactUs.php" > Contact Us </a> </li>
                <li> <a href="logout.php" > Logout </a> </li>

            <?php
            }
            ?>
          </ul>
      </div>
      <!-- End of Menu Bar container -->

      <!-- A text to show the users that they are in My Profile page -->
      <h1> My Profile </h1>

      <!-- Button that will direct the user to their profile -->
      <div id = "choicesContainer" >
        <div class = "overview-container">
          <a href="profile.php">
            <img src = "../img/buttons_imgs/overview.jpg" alt = "overview" width = 80 height = 80>
            <p>
              Profile Overview
            </p>
          </a>
        </div>

        <!-- Button that will direct the user to the the page to update their profile -->
        <div class = "update-container">
          <a href="updateProfile.php">
            <img src = "../img/buttons_imgs/update.jpg" alt = "update" width = 80 height = 80>
            <p>
              Update Profile
            </p>
          </a>
        </div>

        <!-- Button that will direct the user to the the page to where they can create the blog-->
        <div class = "createBlog-container">
          <a href="createBlog.php">
            <img src = "../img/buttons_imgs/create.jpg" alt = "createBlog" width = 80 height = 80>
            <p>
              Create Blog
            </p>
          </a>
        </div>

        <!-- FUTURE functionality: Button that will direct the user to the the page to update their profile -->
        <div class = "myBlog-container">
          <a href="inProgress.php">
            <img src = "../img/buttons_imgs/myBlog.jpg" alt = "myBlog" width = 80 height = 80>
            <p>
              My blog
            </p>
          </a>
        </div>

        <!-- FUTURE functionality:  Button that will direct the user to the the Followers page to list all their followers-->
        <div class = "followers-container">
          <a href="inProgress.php">
            <img src = "../img/buttons_imgs/follow.jpg" alt = "followers" width = 80 height = 80>
            <p>
              Followers
            </p>
          </a>
        </div>

        <!-- FUTURE functionality: Button that will direct the users to the page that lists all the users they follow-->
        <div class = "favoriteBloggers-container">
          <a href="inProgress.php">
            <img src = "../img/buttons_imgs/favorites.jpg" alt = "favorites" width = 80 height = 80>
            <p>
              Favorite Bloggers
            </p>
          </a>
        </div>

      </div>

    </div>
  </body>
</html>
