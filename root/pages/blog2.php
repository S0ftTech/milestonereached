 <!--

Assignment 2: Business Web Technology
Author: Stephen Mina
Student Number: 17072290

Purpose:
The purpose of this php file is to structure all the information that must be presented to a user or guest when they are reading a particular Blog web page
-->


<?php

   // Create or find an existing session
    session_start();

   // To present any messages within the page
   // if (isset($_SESSION["message"]))
   // {
   //      $message .= $_SESSION["message"];
   //      unset($_SESSION["message"]);
   //  }
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <!-- Name of the Webpage -->
    <title> Reading Blog</title>

    <link rel="stylesheet" type = "text/css" href="../css/main.css" > <!-- Main style across the website -->
    <link rel="stylesheet" type = "text/css" href="../css/blogStyle.css"> <!-- Local Style for the Blog page -->
</head>


<body>

  <!-- Blog page container -->
  <div class ="mainContainer">

    <!-- Menu Bar's container -->
    <div class = "menu-bar">

        <!-- Milestone  Reached logo -->
        <img src="../img/logo.png" alt="Milestone Reached logo" width = 49 height = 49>

        <!-- Navigation Bar -->
        <ul class = "menuList">
          <?php
            // If user is not logged in
            if (!(isset($_SESSION['user'])))
            {
           ?>
              <!-- Don't show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="register.php" > Register </a> </li>
              <li> <a href="login.php" > Login </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
          <?php
          }
          else // If user is logged in
          {
          ?>
              <!-- Show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="myProfile.php" > My Profile </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
              <li> <a href="logout.php" > Logout </a> </li>

          <?php
          }
          ?>
        </ul>

    </div>
    <!-- End of Menu bar container-->

    <!-- The Blog details within the page -->
    <div class = "headers">
      <h1> Disney Land </h1>
      <h2> Hui Lian Yoaw </h2>
      <h3> Shanghai, China </h3>
      <h5> The First Disneyland in China. Where it is a land of fantasy </h5>

    </div>

    <!-- This container is to put all the experience and information that the bloggers want to share everyone -->
    <div class = "blogContainer">

      <p>
        Once upon a time in a far far away country, A young lady decided to travel by herself to Disney Land to meet the other princesses
      </p>


    </div>

  </div>


</body>



</html>
