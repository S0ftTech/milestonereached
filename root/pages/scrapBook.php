<!--

Assignment 2: Business Web Technology
Author: Stephen Mina
Student Number: 17072290

Purpose:
The purpose of this php file is to structure all the information of all the blogs that was created by users
to the guests and when they click it, they will be directed to the Blog Page (blog.php) to get more story about the blog
of a user
-->

<?php
   // Create or find an existing session
    session_start();

  //  To show the users when there are any errors within the page
   // if (isset($_SESSION["message"]))
   // {
   //      $message .= $_SESSION["message"];
   //      unset($_SESSION["message"]);
   //  }
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <!-- To show the users that they are in Scrapbook page -->
  <title>
    Scrapbook
  </title>

  <link rel="stylesheet" type = "text/css" href="../css/main.css" > <!-- Main style across the website -->
  <link rel="stylesheet" type = "text/css" href="../css/scrapBookStyle.css" > <!-- Style locally for scrapBook page-->
</head>

<body>

  <!-- Scrapbook container to hold all the information within the scrapbook page -->
  <div class = "main-container">

    <!-- Menu Bar's container -->
    <div class = "menu-bar">

        <!-- Milestone  Reached logo -->
        <img src="../img/logo.png" alt="Milestone Reached logo " width = 49 height = 49>

        <!-- Navigation Bar -->
        <ul class = "menuList">
          <?php
            // If user is not logged in
            if (!(isset($_SESSION['user'])))
            {
           ?>
              <!-- Don't show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="register.php" > Register </a> </li>
              <li> <a href="login.php" > Login </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
          <?php
          }
          else // If user is logged in
          {
          ?>
              <!-- Show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="myProfile.php" > My Profile </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
              <li> <a href="logout.php" > Logout </a> </li>

          <?php
          }
          ?>
        </ul>
    </div>
    <!-- End of Menu Bar container -->

    <!-- Scrapbook Welcome Texts -->
    <div class = "headers" >
      <h1>Scrapbooks</h1>
    </div>
    <!-- End of Welcome Texts -->

    <!-- A structure to store the information of all the blogs that was created by users and present them to the guest and otehr users -->
    <div class = "scrapBook-container">

      <!-- A List that will go through all the blogs that exist and print them out across (left to right) -->
      <ul>
        <li>
          <!-- <div class = "blogContainer"> -->
            <img src="../img/blog_imgs/foodAdventure.jpg" alt="foodAdv1" height="150" width="150">
            <h2> Food Adventure </h2>
            <h3> Taiwan </h3>
            <p>
              For people who loves to eat, here are some recommendations
            </p>

            <a href = "blog.php"> Read More </a> <!-- Link of the blog -->
          <!-- </div> -->
        </li>

        <li>
          <!-- <div class = "blogContainer"> -->
            <img src="../img/blog_imgs/disneyland.jpg" alt="disneyLand" height="150" width="150">
            <h2> Disney Land </h2>
            <h3> Shanghai, China </h3>
            <p>
              The First Disneyland in China. Where it is a land of fantasy
            </p>

            <a href = "blog2.php"> Read More </a> <!-- Link of the blog -->
          <!-- </div> -->
        </li>
        <li>
          <img src="../img/blog_imgs/wkendGetaway.jpg" alt="melbAust" height="150" width="150">
          <h2> Weekend Getaway </h2>
          <h3> Melbourne, Australia </h3>
          <p>
            Walking by the streets
          </p>

          <a href = "blog3.php"> Read More </a> <!-- Link of the blog -->
        </li>

      </ul>
    </div>


  </div>
  <!-- End of the main-container -->

</body>
</html>
