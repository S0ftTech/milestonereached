<!--

Assignment 2: Business Web Technology
Author: Stephen Mina
Student Number: 17072290

Purpose:
The purpose of this php file is to structure the Popular post webpage of the milestonereached website
This will give the users or guests information about the the mostly viewed and liked blogs and within this blogs
there will be a link  that will direct them to the blog.php to show the associated information

Future Functionalities:
To have a working link to direct the users into the right blog page associated to the particular blog
-->

<?php

   // Create or find an existing session
    session_start();

  //  To show any error messages within the page
   // if (isset($_SESSION["message"]))
   // {
   //      $message .= $_SESSION["message"];
   //      unset($_SESSION["message"]);
   //  }
?>

<!DOCTYPE html>
<html lang="en">
<head>

  <!-- To show the users that they are in Popular Post page -->
  <title>
    Popular Posts
  </title>

  <link rel="stylesheet" type = "text/css" href="../css/main.css" > <!-- Main style across the website -->
  <link rel="stylesheet" type="text/css" href="../css/popularPostStyle.css"> <!-- Main style locally for Popular Post page -->

</head>

<body>

  <div class = "main-container">

    <!-- Menu Bar's container -->
    <div class = "menu-bar">

        <!-- Milestone  Reached logo -->
        <img src="../img/logo.png" alt="Milestone Reached logo " width = 49 height = 49>

        <!-- Navigation Bar -->
        <ul class = "menuList">

          <?php
            // If user is not logged in
            if (!(isset($_SESSION['user'])))
            {
           ?>
              <!-- Don't show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="register.php" > Register </a> </li>
              <li> <a href="login.php" > Login </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
          <?php
          }
          else // If user is logged in
          {
          ?>
              <!-- Show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="myProfile.php" > My Profile </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
              <li> <a href="logout.php" > Logout </a> </li>

          <?php
          }
          ?>
        </ul>
    </div>
    <!-- End of Menu Bar container -->


    <!-- A text to show the users that they are in Popular Posts page -->
    <div class = "headers">
      <h1> Popular Posts </h1>
    </div>

    <!-- To display the mostly viewed blog within the milestonereached website -->
    <div class = "most-viewed">
      <h2> Most viewed: </h2>

      <img src="../img/blog_imgs/disneyland.jpg" alt = "disneyLand" height = "150" weight = "150">

      <h3> Hui Lian Yoaw </h3>

      <h4> Shanghai, China</h4>

      <p>
        The First Disneyland in China. Where it is a land of fantasy
      </p>

      <a href="blog2.php"> Read More </a> <!--- link to the blog page-->
    </div>


    <!-- To display the mostly liked blog within the milestonereached website -->
    <div class = "most-liked">
      <h2> Most liked: </h2>

      <img src="../img/blog_imgs/wkendGetaway.jpg" alt="melbAust" height="150" width="150">

      <h3> Hui Lian Yoaw </h3>

      <h4>Melbourne, Australia</h4>

      <p>
        Walking by the streets
      </p>

      <a href="blog3.php"> Read More </a> <!--- link to the blog page-->
    </div>


  </div>



</body>
</html>
