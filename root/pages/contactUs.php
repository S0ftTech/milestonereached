<!--

Assignment 2: Business Web Technology
Author: Stephen Mina
Student Number: 17072290

Purpose:
The purpose of this php file is to structure all the information that must be presented to a user or guest when they want to contact the Milestone reached authors
-->

<?php
   // Create or find an existing session
    session_start();

   // To present any messages within the page
   // if (isset($_SESSION["message"]))
   // {
   //      $message .= $_SESSION["message"];
   //      unset($_SESSION["message"]);
   //  }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>
    Contact Us
  </title>
  <meta charset="utf-8" />

  <link rel="stylesheet" type = "text/css" href="../css/main.css" > <!-- Main style across the website -->
  <link rel="stylesheet" type = "text/css" href="../css/contactUsStyle.css"> <!-- Local Style for the Contact Us page -->
  <script async defer
  src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZDhBgLN0pYopBCU-LDtAo9ruEjsAO-FE&callback=initMap">
  </script>
  <script rel="text/javascript" src ="../js/maps.js"></script>

</head>

<body>

    <!-- Menu Bar's container -->
    <div class = "menu-bar">

      <!-- Milestone  Reached logo -->
      <img src="../img/logo.png" alt="Milestone Reached logo " width = 49 height = 49>

        <!-- Navigation Bar -->
        <ul class = "menuList">
          <?php
            // If user is not logged in
            if (!(isset($_SESSION['user'])))
            {
           ?>
              <!-- Don't show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="register.php" > Register </a> </li>
              <li> <a href="login.php" > Login </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
          <?php
          }
          else // If user is logged in
          {
          ?>
              <!-- Show logout and MyProfile page -->
              <li> <a href="../index.php" > Home </a> </li>
              <li> <a href="myProfile.php" > My Profile </a> </li>
              <li> <a href="scrapBook.php" > Scrapbooks </a> </li>
              <li> <a href="destinations.php" > Destinations </a> </li>
              <li> <a href="popularPost.php" > Popular posts </a> </li>
              <li> <a href="contactUs.php" > Contact Us </a> </li>
              <li> <a href="logout.php" > Logout </a> </li>

          <?php
          }
          ?>
        </ul>
    </div>

    <!-- To show the users that they are in Contact Us page -->
    <div class = "headers">

      <h1>Contact Us</h1>
    </div>

    <!-- The Contact details within the page -->
    <div class = "main-container" >
      <div class = "contacts" >

        <div class = "details">
          <h3> Address: </h3>
          <p> 536 Gore Street , Bentley, Perth WA, 6102 </p>
          <h3> Email: </h3>
          <p> <emp> milestoneReached@gmail.com  </emp> </p>

          <!-- This is the container to display map coordinates -->
          <div id = "branchMap"></div>
        </div>


      </div>
    </div>
</body>
</html>
