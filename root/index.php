<?php
      // //Reload with HTTPS if necessary
      // if (empty($_SERVER['HTTPS'])) {
      //   header("Location: https://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);
      //   exit;
      // }

   // Create or find an existing session
    session_start();

   // Possible logout/can't authenticate message
   if (isset($_SESSION["message"]))
   {
        $message .= $_SESSION["message"];
        unset($_SESSION["message"]);
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>
    Home
  </title>

  <link rel="stylesheet" type = "text/css" href="css/main.css" >  <!-- Main style across the website -->
  <link rel="stylesheet" type = "text/css" href="css/homeStyle.css" > <!-- Local Style for the homepage -->
  <meta charset="UTF-8">
</head>

<body>

    <!-- Homepage container -->
    <div class = "mainContainer">

      <!-- Menu Bar's container -->
      <div class = "menu-bar">

          <!-- Milestone  Reached logo -->
          <img src="img/logo.png" alt="Milestone Reached logo " width = 49 height = 49>

          <!-- Navigation Bar -->
          <ul class = "menuList">
            <?php
              // If user is not logged in
              if (!(isset($_SESSION['user'])))
              {
             ?>
                <!-- Don't show logout and MyProfile page -->
                <li> <a href="index.php" > Home </a> </li>
                <li> <a href="pages/register.php" > Register </a> </li>
                <li> <a href="pages/login.php" > Login </a> </li>
                <li> <a href="pages/scrapBook.php" > Scrapbooks </a> </li>
                <li> <a href="pages/destinations.php" > Destinations </a> </li>
                <li> <a href="pages/popularPost.php" > Popular posts </a> </li>
                <li> <a href="pages/contactUs.php" > Contact Us </a> </li>
            <?php
            }
            else // If user is logged in
            {
            ?>
                <!-- Show logout and MyProfile page -->
                <li> <a href="index.php" > Home </a> </li>
                <li> <a href="pages/myProfile.php" > My Profile </a> </li>
                <li> <a href="pages/scrapBook.php" > Scrapbooks </a> </li>
                <li> <a href="pages/destinations.php" > Destinations </a> </li>
                <li> <a href="pages/popularPost.php" > Popular posts </a> </li>
                <li> <a href="pages/contactUs.php" > Contact Us </a> </li>
                <li> <a href="pages/logout.php" > Logout </a> </li>

            <?php
            }
            ?>
          </ul>

      </div>
      <!-- End of Menu bar container-->

        <!-- Welcome Texts -->
        <div class = "headers" >
          <h1>Milestone Reached</h1>
          <h2> Today's Top post </h2>
        </div>
        <!-- End of Welcome Texts -->

        <!-- Top Blog Container -->
        <div class = 'topBlogContainer'>
          <!-- Top blog's Image -->
          <img src = "img/PerthCity.jpg" alt = "topBlogsImage">

          <!-- Top blog's Hoverable Title -->
          <div class ='topBlogTitle'>
            <div class = 'text'>
              Food Adventure
            </div>
          </div>

          <!-- Top Blog's details -->
          <div class = 'topBlogDetails'>

            <h3> Hui Lian Yoaw </h3>  <!-- Blogger -->
            <h4> Taiwan </h4>  <!-- Place -->
            <p>
              For people who loves to eat, here are some recommendations
            </p>

            <a href = "pages/blog.php"> Read More </a> <!-- Link of the blog -->
          </div>
        </div>
        <!-- End of Top blog container -->

    </div>


</body>
</html>
