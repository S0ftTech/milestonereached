-- Assignment 2: Business Web Technology
-- Author: Stephen Mina
-- Student Number: 17072290
--
-- Purpose:
-- The purpose of this sql file is for creating the tables needed for the milestonereached website when a database named milestonereached has been created.

-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 26, 2018 at 06:22 PM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `milestonereached`
--

-- --------------------------------------------------------

--
-- Table structure for table `userblogs`
--

CREATE TABLE IF NOT EXISTS `userblogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(20) NOT NULL,
  `title` varchar(150) COLLATE utf8_general_mysql500_ci NOT NULL,
  `location` varchar(150) COLLATE utf8_general_mysql500_ci NOT NULL,
  `summary` varchar(200) COLLATE utf8_general_mysql500_ci NOT NULL,
  `story` text COLLATE utf8_general_mysql500_ci NOT NULL,
  `photo` varchar(150) COLLATE utf8_general_mysql500_ci NOT NULL,
  `video` varchar(150) COLLATE utf8_general_mysql500_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` varchar(150) COLLATE utf8_general_mysql500_ci NOT NULL,
  `firstname` varchar(100) COLLATE utf8_general_mysql500_ci NOT NULL,
  `lastname` varchar(100) COLLATE utf8_general_mysql500_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_general_mysql500_ci NOT NULL,
  `contacts` varchar(400) COLLATE utf8_general_mysql500_ci DEFAULT NULL,
  `about` text COLLATE utf8_general_mysql500_ci,
  `photo` varchar(100) COLLATE utf8_general_mysql500_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci AUTO_INCREMENT=33 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `userblogs`
--
ALTER TABLE `userblogs`
  ADD CONSTRAINT `userblogs_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
