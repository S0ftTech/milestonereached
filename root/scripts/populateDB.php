<!--
Assignment 2: Business Web Technology
Author: Stephen Mina
Student Number: 17072290

Purpose:
The purpose of this php file is for new users to be able to create the database for milestonereached website and populate it with a user and a blog associated to him/her

 -->

<?php

	//Host attributes
	$hostname = "localhost";
	$database = "milestonereached";
	$dbusername = "root";
	$dbpassword = "Password1234";

	// Check whether we can connect to the database
	if(!($connection = @ mysql_connect($hostname, $dbusername, $dbpassword)))
	{
		echo "connection failed";
		die("Could not connect to the database. Please try again later.\n");
	}
	else
	{
	  	echo "Connection is ok\n";
	}

	// Check whether database exists
	if (!mysql_select_db($database, $connection))
	{
		echo "Database not selected\n";

	}
	else
	{
		echo "Database is connected\n";
	}

	// Put an sample user into the database
	$sql = "INSERT INTO `users` (`email`, `firstname`, `lastname`, `password`, `contacts`, `about`, `photo`) VALUES (\"hly@gmail.com\", \"Hui Lian\", \"Yoaw\", \"Password1234\", \"Gmail: hly@gmail.com Instragram: @hly_\", \"I am a frequent traveller who loves photography\", \"profile1.jpg\")";

	// FOR FUTURE use: Tried to set up a checker to only add a user when their information has not been registered into the database

					// $sql = "INSERT INTO `users` (`email`, `firstname`, `lastname`, `password`, `contacts`, `about`, `photo`)
					// SELECT * FROM (SELECT "hly@gmail.com", "Hui Lian", "Yoaw", "Password1234", "Gmail: hly@gmail.com Instragram: @hly_", "I am a frequent traveller who loves photography", "profile1.jpg") AS tmp
					// WHERE NOT EXISTS (
					// 	SELECT `email` FROM `users` WHERE `email` = 'hly@gmail.com'
					// ) LIMIT 1;"

	// If the fields are satisfied, add the user to the database
	if(! ($result = @ mysql_query ($sql)))
	{
		echo 'User Not Inserted\n';

	}
	else
	{
		echo 'User data has been inserted succesfully';

		// Once added, let's create a sample blog to this associated user
		$sql = "SELECT * FROM users where email = 'hly@gmail.com' ";

		// Call this function to return us a unique query from our database
		$result = mysql_query($sql,$connection);

		// Retrieve the number of rows
		$rows = mysql_num_rows($result);
    if ($rows > 0)
    {
			// Get the user's id
			// echo "This is the row id:" .$rows["id"];
			while($row = mysql_fetch_assoc($result)){
        $id = $row["id"];

      }

			// Using the USER's ID, we will be adding it into the userblogs table and create a blog for him or her
			$sql = "INSERT INTO `milestonereached`.`userblogs` (`userid`,`title`, `location`, `summary`, `story`, `photo`, `video`)
			VALUES ( \"$id\" ,\"Food Adventure\", \"Taiwan\", \"For people who loves to eat, here are some recommendations\", \"Once upon a time in a far far away country, two restaurants have been battling each other to provide the best food in their town. Then, one day, a woman have decided to travel on the country and try both of the restaurant's dishes \", \"foodAdventure.jpg\", \"food_adventure.mp4\")";

			// If the fields are satisfied, add the user to the database
			if(! ($result = @ mysql_query ($sql)))
			{
				echo 'Blog Not Inserted\n';
				echo 'Error with my query : '.$sql;
    		echo mysql_error();
			}
			else
			{
				echo 'Blog data has been inserted succesfully';
			}
		}
    else
       return false;

	}

?>

<!--
References:
These code were created from the help of the Module 7's laboratory: world zip file: https://lms.curtin.edu.au/webapps/blackboard/content/listContent.jsp?course_id=_83258_1&content_id=_5427102_1
PHP.2018. 'mysql_query'.PHP. http://php.net/manual/en/function.mysql-query.php
-->
