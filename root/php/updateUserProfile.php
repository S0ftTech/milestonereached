<!--

Assignment 2: Business Web Technology
Author: Stephen Mina
Student Number: 17072290

Purpose:
The purpose of this php file is to structure handle the information of the users to update them and store them into the database
and return a message to inform the user if they have updated their profile successfully or not.

Future Functionalities:
To have an image and video uploader that will update the image/videos chosen by the user into our 'img' and 'video' directory using the commented file in line 20-32
-->
<?php
 require 'db.php';
 // Check for any existing sessions
 session_start();

$id = $_SESSION['id'];
$newFirstname = $_POST['updateFirstname'];
$newLastname = $_POST['updateLastname'];
$newEmail = $_POST['updateEmail'];
$newPassword = $_POST['updatePassword'];
$newAbout = $_POST['updateAbout'];
$newContact = $_POST['updateContact'];
$newPhoto = $_POST['updatePhoto'];

// echo $newFirstname;
// echo $newLastname;
// echo $newEmail;
// echo $newPassword;
// echo $newAbout;
// echo $newContact;
// echo $newPhoto;

// FOR FUTURE PLAN, IMPLEMENT A IMAGE and Video UPLOADER TO KEEP TRACK OF THE PROFILE PICTURE and videos OF THE USER
// if (isset($_POST["profilePhoto"]))
// {
//   $file = $_FILES["file"];
//
//   $fileName = $file["name"];
//   $fileTempLoc = $_file["tmp_name"];
//   // if(move_uploaded_file($fileName,"../img/profile_imgs/".$fileName))
//   // {
//   //   $img_message = $img_message . "<p>The image has been uploaded </p>"
//   // }
//     $img_message = $img_message . "<p>The image has been uploaded </p>"
// }

// Update the information provided by the user

$sql = "UPDATE `users`
        SET `firstname`= '$newFirstname',
            `lastname`= '$newLastname',
            `email` = '$newEmail',
            `password` = '$newPassword',
            `contacts` = '$newContact',
            `about` = '$newAbout',
            `photo` = '$newPhoto'
        WHERE `id` = '$id'";

// update data into the database
 if(! ($result = @ mysql_query ($sql)))
 {
   echo 'User Not Inserted\n';
   $message = $message . "<p>Please make sure all fields are correct </p>";
 }
 else
 {
   echo 'User data has been updated succesfully';
   $message = $message . "You have been updated your information successfully  </p>";

    $_SESSION['firstname']= $newFirstname;
    $_SESSION['lastname']= $newLastname;
    $_SESSION['user'] = $newEmail;
    $_SESSION['password'] = $newPassword;
    $_SESSION['contacts'] = $newContact;
    $_SESSION['about'] = $newAbout;
    $_SESSION['profilePhoto'] = $newPhoto;
 }

 // store any messages to be show the user if they have updated successfully
 $_SESSION['update_message'] = $message;
 $_SESSION['update_image_message'] = $img_message;
 header("Location: ../pages/updateProfile.php");

?>
