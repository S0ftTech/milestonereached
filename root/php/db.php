<!--

Assignment 2: Business Web Technology
Author: Stephen Mina
Student Number: 17072290

Purpose:
The purpose of this php file is for our milestonereached website to make a connection to our database
This is usually used when we want to access information within our database when users want to register (registerUser.php),
login (logincheck.php) to create blog and show all the relevant information to ceratin pages within the milestonereached website
-->

<?php

	//Host attributes
	$hostname = "localhost";
	$database = "milestonereached";
	$dbusername = "root";
	$dbpassword = "Password1234";

	// Making a connection
	if(!($connection = @ mysql_connect($hostname, $dbusername, $dbpassword)))
	{
		echo "connection failed";
		die("Could not connect to the database. Please try again later.\n");
	}
	else
	{
	  	echo "Connection is ok\n";
	}

	// To check whether a database exist
	if (!mysql_select_db($database, $connection))
	{
		echo "Database not selected\n";

	}
	else
	{
		echo "Database is connected\n";
	}

  // Establish and return a connection to the mySQL server
  function get_mysql_connection()
  {
   global $hostname;
   global $dbusername;
   global $dbpassword;
   global $database;

   // Connect to server
   if (!($connection = @ mysql_connect($hostname, $dbusername, $dbpassword)))
      die("Could not connect to the database. Please try again later.");

   // Select the database
   if (!mysql_select_db($database, $connection))
      showerror();

   return $connection;
  }

  // Print an error number and message in red.
  function showerror()
  {
    echo '<FONT COLOR="#FF0000"><B>';
    die("Error " . mysql_errno() . "</B> : " . mysql_error() . '</FONT>');
  }



  // Clean the array to prevent security problem with strings
  function mysqlclean ($array, $index, $maxlength, $connection)
  {
    if (isset($array["{$index}"]))
    {
        $input = substr($array["{$index}"], 0, $maxlength);
        $input = mysql_real_escape_string($input, $connection);
        $input = trim($input);
        return ($input);
    }
  }

  // Return a table containing the results of a database query
  function get_table($connection, $query)
  {
       $table=null;
       if (! ($result = @ mysql_query ($query, $connection)))
              showerror();

       // Build an array containing a copy of the table
       $index=0;
       while ($row = @ mysql_fetch_array($result))
       {
         $table[$index] = $row;
         $index++;
       }

       // Return the table resulting from the query
       return $table;
  }

?>

<!--
References
These code were created from the help of the Module 7's laboratory: world zip file: https://lms.curtin.edu.au/webapps/blackboard/content/listContent.jsp?course_id=_83258_1&content_id=_5427102_1

-->
