<!--

Assignment 2: Business Web Technology
Author: Stephen Mina
Student Number: 17072290

Purpose:
The purpose of this file is to create functions to get access information within the database
-->

<?php
  require 'authentication.inc';
  require 'db.php';

  function user_profile($connection, $email, $password)
  {
     $query = "SELECT * FROM users where username = '".$_POST["email"]."' and password = '".$_POST["password"]."'";
     if (!(@mysql_query($query, $connection)))
          showerror();
  }

  $connection = get_mysql_connection();

  // Clean the data collected in the form
  $email    = mysqlclean($_POST, "email",    64, $connection);
  $password = mysqlclean($_POST, "password", 32, $connection);

  session_start();

  // Authenticate the user
  if (authenticate_user($connection, $email, $password))
  {
     // Register the loginUsername
      $_SESSION["user"] = $email;

     // Register the IP address that started this session
      $_SESSION["password"] = $password;

      // Look for the user, using ther email
      $sql = "SELECT * FROM users where email = '".$_SESSION["user"]."'";

      // $sql = "SELECT * FROM users where email=\"$email\";";
      // $result = mysql_query($connection,$sql);
      $result = mysql_query($sql,$connection);

      // $rows = mysql_num_rows($result);

      // If there is a user, get their details
      while($row = mysql_fetch_assoc($result)){
        $_SESSION["firstname"] = $row["firstname"];
        $_SESSION["lastname"] = $row["lastname"];
        $_SESSION["about"] = $row["about"];
        $_SESSION["contacts"] = $row["contacts"];
        $_SESSION["profilePhoto"] = $row["photo"];
        $_SESSION["id"] = $row["id"];
      }

        // Acquire the associated user's blogs
        $sqlBlog = "SELECT * FROM userblogs where userid = '".$_SESSION["id"]."'";
        $result = mysql_query($sqlBlog,$connection);

        // Retrieve the number of rows
        $rows = mysql_num_rows($result);
        $_SESSION["rows"] = $rows;
        // If they have created blogs go through them
        if ($rows > 0)
        {
          // Get the user's id
          while($row = mysql_fetch_array($result))
          {
            $_SESSION["title"] = $row[2];
            $_SESSION["location"] = $row[3];
            $_SESSION["summary"] = $row[4];
            $_SESSION["blogPhoto"] = $row[6];

          }
        //If there are no blogs, just leave empty the blog category empty

        }
      // direct them to the their profile
     header("Location: ../pages/profile.php");
     exit();
  }
  else
  {
     // The authentication failed: setup a logout message
     $_SESSION["message"] =
       "The '{$email}' or password provided is incorrect";

     // Direct them to the login page and inform them that they have provided a wrong login credentials
     header("Location: ../pages/login.php");
     exit();
  }
?>
