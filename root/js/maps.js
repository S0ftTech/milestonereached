/* Assignment 2: Business Web Technology
Author: Stephen Mina
Student Number: 17072290

Purpose:
The purpose of this js file is to set the coordinates of the milestonereached branch location */

function initMap() {
  var options = {
    zoom: 8,
    center:{ lat: -32.001, lng: 115.924 } //Set thje coordinates of the particular branch location
  }
  var map = new google.maps.Map(document.getElementById('branchMap'), options);

  var marker = new google.maps.Marker({
    position:{lat: -32.001, lng: 115.924}, //Set the marker on the same coordinates
    map:map
  });

}
