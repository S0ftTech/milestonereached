The steps provided below will set the milestonereached website:

STEP 1: CREATE THE DATABASE
1.) Go to localhost/phpmyadmin
2.) Login details are:
	username: root
	password: Password1234
3.) Create a database named milestonereached and click milestonereached Database within the phpmyadmin page

STEP 2: 'IMPORT' THE TABLES
1.) Now that the database has been created, go to the milestonereached or root directory of this assignment
2.) Go to the scripts directory (located under root/scripts/)
3.) IMPORT the milestonereached.sql file into the phpmyadmin milestonereached database
4.) This should create the associated tables for you

STEP 3: 'POPULATE' THE TABLES
1.) Now that we have the tables created
2.) Open a browser and type in localhost/root/scripts/populateDB.php
3.) This php file should print something within the webpage that informs you that users and blogs have been inserted successfully.

STEP 4: Use Milestonereached website as a GUEST
1.) Open a browser
2.) Type localhost/root/ (this should show you the homepage of milestonereached )
3.) Browse the sample website (as a guest) for the milestonereached website

To Get the Milestonereached website to work as a REGISTERED USER
1.) Open a browser
2.) Type localhost/root/ (this should show you the homepage of milestonereached )
3.) Head over to the Register page and provide your details (The fields: "About", "Contacts" and photo are optional)
	3.1) Or you could use the sample user account created for you:
				Email: hyl@gmail.com
				Password: Password1234
4.) Once logged in, this will take you to the profile page
